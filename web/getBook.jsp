<%--
  Created by IntelliJ IDEA.
  User: pengzhe
  Date: 2018/11/8
  Time: 15:39
  To change this template use File | Settings | File Templates.
--%>

<%@ page import="java.sql.*" %>
<%@ page import="DAO.BookDao" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<%
    BookDao dao =new BookDao();
    String sql = "select id,name,author,price,image,description,category_id from book" +
            " where category_id = ?";
    String categoryID = request.getParameter("id");
    PreparedStatement pstat = dao.getConnection().prepareStatement(sql);
    pstat.setString(1, categoryID);
    ResultSet rs = pstat.executeQuery();
    while (rs.next()) {
        //String url1 = "buyBook.jsp?name="+rs.getString("name");
        String name = rs.getString("name");
%>
<div class="col-md-3">
    <div class="thumbnail">
        <img src="<%=rs.getString("image")%>" style="height: 200px">
        <div class="caption" style="height: 330px">
            <h4>
                <%=rs.getString("name")%>
            </h4>
            <p style="height: 220px">
                <%=rs.getString("description")%>
            </p>
            <p>
                <a href="javascript:showname(<%=rs.getString("id")%>)" class="btn-primary" role="button">加入购物车</a>
            </p>
        </div>
    </div>
</div>
<%
    }
    pstat.close();
    dao.close();
%>

</body>
</html>
