<%--
  Created by IntelliJ IDEA.
  User: pengzhe
  Date: 2018/11/8
  Time: 15:14
  To change this template use File | Settings | File Templates.
--%>

<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="DAO.BookDao" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <link href="css/style.css" rel="stylesheet"/>
    <title>网上书店</title>

    <script language="JavaScript">
        function showBook(categoryID) {
            xmlHttp.open("GET", "getBook.jsp?id=" + categoryID, true);
            xmlHttp.onreadystatechange = function () {
                if (xmlHttp.readyState == 4) {
                    if (xmlHttp.status == 200) {
                        var data = xmlHttp.responseText;
                        document.getElementById("book").innerHTML = data;
                    }
                }
            }
            xmlHttp.send();
        }

        function showname(id) {
            $.ajax({
                type: 'POST',
                url: 'servlet/ShopServlet',
                data: {
                    id: id
                }
            });
        }
    </script>
</head>
<body onload="initAJAX()">
<%
    if (session.getAttribute("userName") == null) {
        ArrayList bookids = new ArrayList();
        session.setAttribute("bookids", bookids);
        ArrayList bookname = new ArrayList();
        session.setAttribute("bookname", bookname);
        ArrayList bookprice = new ArrayList();
        session.setAttribute("bookprice", bookprice);
        ArrayList finalnumber = new ArrayList();
        session.setAttribute("finalnumber", finalnumber);
    }
%>
<div class="header">
    <div class="container">
        <div class="row">
            <div class="login span4">
                <h1><a href=""> 欢迎来到<strong>我的</strong>书店</a>
                    <span class="red">
                        ${ userName eq null ? " ":userName}
                    </span>
                    <a href="buyBook.jsp">购物车</a>
                    <a href="order.jsp">查看订单</a>
                </h1>
            </div>
            <div class="links span8">
                <%
                    if (session.getAttribute("userName") == null) {
                %>

                <a class="login" href="login.html" rel="tooltip" data-placement="bottom" data-toggle="modal"
                   data-target="#myModal"></a>
                <a class="register" href="register.html" rel="tooltip" data-placement="bottom" data-toggle="modal"
                   data-target="#myModal"></a>
                <%
                } else {
                %>
                <form action="servlet/SetoutServlet" method="get">
                    <input type="submit" value="注销">
                </form>
                <%}%>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row"> <%--下方左右div控制--%>
        <div class="col-md-3"> <%--左侧菜单div控制--%>
            <ul class="nav nav-list">
                <li class="nav-header">书籍类型</li>
                <%
                    BookDao dao = new BookDao();
                    String sql = "select id,name,description from category ";

                    Statement stat =dao.getConnection().createStatement();
                    ResultSet rs = stat.executeQuery(sql);
                    while (rs.next()) {
                %>
                <li>
                    <a href="javascript:showBook(<%=rs.getString("id")%>)"><%=rs.getString("name")%>
                    </a>
                </li>
                <%
                    }
                    stat.close();
                    dao.close();
                %>
            </ul>
        </div>
        <%--左侧菜单div控制--%>
        <div class="col-md-9" id="book"><%--书籍布局控制--%>

        </div>
        <div id="buybook"></div>
    </div>
</div>
<%--下方左右div控制--%>
<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"></div>
            <div class="modal-body"></div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/commons.js"></script>
</body>
</html>
