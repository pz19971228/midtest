package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class BookDao {
    private Connection conn = null ;

    public BookDao(){
       try{
           Class.forName("org.sqlite.JDBC");
           String url = "jdbc:sqlite::resource:bookstore.db";
           this.conn = DriverManager.getConnection(url);
       }catch (ClassNotFoundException e){
           e.printStackTrace();
       }catch (SQLException e){
           e.printStackTrace();
       }
    }

    public Connection getConnection(){
        return  this.conn ;
    }

    public void close(){
        try{
            this.conn.close() ;
        }catch (Exception e){
            System.out.println("数据库连接关闭失败");
            e.printStackTrace();
        }
    }
}
