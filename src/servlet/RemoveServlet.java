package servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "RemoveServlet")
public class RemoveServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        ArrayList bookids = (ArrayList) session.getAttribute("bookids");
        ArrayList bookname = (ArrayList) session.getAttribute("bookname");
        ArrayList bookprice = (ArrayList) session.getAttribute("bookprice");
        String key = request.getParameter("key");
        int i = Integer.parseInt(key);
        bookids.remove(i);
        bookname.remove(i);
        bookprice.remove(i);
        response.sendRedirect("/buyBook.jsp");
    }
}
