package servlet;

import DAO.BookDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

@WebServlet(name = "AddBookServlet")
public class AddBookServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        String name = request.getParameter("name");
        String author = request.getParameter("author");
        String pri= request.getParameter("price");
        String image = request.getParameter("image");
        String description = request.getParameter("description");
        Double price = Double.parseDouble(pri);
        String category_id = request.getParameter("category_id");
        BookDao dao = new BookDao();
        PreparedStatement statement = null;
        try{
            String sql = "insert into book(name, author, price, image, description, category_id) values (?,?,?,?,?,?)";
            statement = dao.getConnection().prepareStatement(sql);
            statement.setString(1, name);
            statement.setString(2, author);
            statement.setDouble(3, price);
            statement.setString(4, image);
            statement.setString(5, description);
            statement.setString(6, category_id);
            statement.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            try{
                if(statement != null)
                    statement.close();
                dao.close();
            }catch (SQLException e){
                e.printStackTrace();
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
