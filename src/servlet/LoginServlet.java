package servlet;

import DAO.BookDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@WebServlet(name = "LoginServlet")
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String userName = request.getParameter("user_name");
        String userPassword = request.getParameter("user_password");
        HttpSession session = request.getSession();
        BookDao dao = new BookDao();
        Statement statement = null;
        try {
            statement = dao.getConnection().createStatement();
            //"select name , password from user where name= ' test'||1=1"# 'and password ='gasgasdasa'";
            String sql = "select name , password from user where name= '" + userName + "'and password ='" + userPassword + "'";
            ResultSet rs = statement.executeQuery(sql);
            PrintWriter printWriter = response.getWriter();
            if (rs.next()) {
                if (userName.equals("root")) {
                    response.setContentType("text/html;charset=UTF-8");
                    printWriter.println("登录成功");
                    session.setAttribute("userName", userName);
                    response.sendRedirect("/root.html");
                } else {
                    response.setContentType("text/html;charset=UTF-8");
                    printWriter.println("登录成功");
                    session.setAttribute("userName", userName);
                    response.sendRedirect("/index.jsp");
                }

//                response.setHeader("Refresh", "3; /index.jsp");
            } else {
                response.setContentType("text/html;charset=UTF-8");
                printWriter.println("登录失败,请重试");
                response.setHeader("Refresh", "3; /index.jsp");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null)
                    statement.close();
                dao.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
