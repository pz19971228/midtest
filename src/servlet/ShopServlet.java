package servlet;

import DAO.BookDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

@WebServlet(name = "ShopServlet")
public class ShopServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        BookDao dao = new BookDao();
        Statement stat1 = null;
        String id = request.getParameter("id");
        ArrayList bookids = (ArrayList) session.getAttribute("bookids");
        ArrayList bookprice = (ArrayList) session.getAttribute("bookprice");
        bookids.add(id);
        ArrayList bookname = (ArrayList) session.getAttribute("bookname");
        try {

            String sql = "select name , price from book where id="+id;
            stat1 = dao.getConnection().createStatement();
            ResultSet rs = stat1.executeQuery(sql);
            while (rs.next()){
               bookname.add(rs.getString("name"));
               bookprice.add(rs.getString("price"));
            }

        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            try {
                if(stat1 != null)
                    stat1.close();
                dao.close();
            }catch (SQLException e){
                e.printStackTrace();
            }
        }
//        String number = request.getParameter("number");
//        ArrayList booknumber = (ArrayList) session.getAttribute("booknumber");
//        booknumber.add(number);
//        for(int i =0;i<bookids.size();i++){
//            System.out.println("书的id"+bookids.get(i)+"数量是"+booknumber.get(i));
//        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
