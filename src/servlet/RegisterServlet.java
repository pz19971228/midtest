package servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

@WebServlet(name = "RegisterServlet")
public class RegisterServlet extends HttpServlet {
    Connection connection = null;
    Statement statement = null;

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            Class.forName("org.sqlite.JDBC");
            String url = "jdbc:sqlite::resource:bookstore.db";
            connection = DriverManager.getConnection(url);
            statement = connection.createStatement();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void destroy() {
        super.destroy();
        try {
            if (statement != null)
                statement.close();
            if (connection != null)
                connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String userName = request.getParameter("user_name");
        String userPassword = request.getParameter("user_password");
        String userPasswordRe = request.getParameter("user_password_re");
        String userPhone = request.getParameter("user_phone");

        PrintWriter printWriter = response.getWriter();
        if (userName.length() == 0) {
            System.out.println("请求的用户名为空");
            response.setContentType("text/html;charset=UTF-8");
            printWriter.println("注册失败,请重试");
            response.setHeader("Refresh", "3; /index.jsp");
        }
        if (userPassword.length() != userPasswordRe.length()) {
            System.out.println("请求中的两次密码不一致");
            response.setContentType("text/html;charset=UTF-8");
            printWriter.println("注册失败,请重试");
            response.setHeader("Refresh", "3; /index.jsp");
        }
        if (!userPassword.equals(userPasswordRe)) {
            System.out.println("请求中的两次密码不一致");
            response.setContentType("text/html;charset=UTF-8");
            printWriter.println("注册失败,请重试");
            response.setHeader("Refresh", "3; /index.jsp");
        }

        String sql = "insert into user (name,password,phone) values('" + userName + "','" + userPassword + "','" + userPhone + "')";
        try {
            statement.executeUpdate(sql);
            response.sendRedirect("/index.jsp");
            HttpSession session = request.getSession();
            session.setAttribute("userName", userName);

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
