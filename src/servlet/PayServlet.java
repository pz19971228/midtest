package servlet;

import DAO.BookDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.text.SimpleDateFormat;

@WebServlet(name = "PayServlet")
public class PayServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        ArrayList finalnumber = (ArrayList) session.getAttribute("finalnumber");
        ArrayList bookprice = (ArrayList) session.getAttribute("bookprice");
        ArrayList bookids = (ArrayList) session.getAttribute("bookids");
        String username = (String) session.getAttribute("userName");
        double money = (Double) session.getAttribute("money");
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String ordertime = df.format(new Date());
        PreparedStatement ps = null;
        Statement statement = null;
        BookDao dao = new BookDao();
        try {
            String sql = "select id from user where name = '" + username + "'";
            statement = dao.getConnection().createStatement();
            ResultSet rs = statement.executeQuery(sql);
            String user_id = rs.getString("id");
            String sql1 = "insert into orders (ordertime,price,state,user_id) values('" + ordertime + "','" + money + "','" + 0 + "','" + user_id + "')";
            statement.executeUpdate(sql1);
            String sql2 = "select max(id) from orders";
            ResultSet rs1 = statement.executeQuery(sql2);
            String order_id = rs1.getString("max(id)");
            String sql3 = "insert into orderitem (quantity, price, order_id, book_id) values (?,?,?,?)";
            ps = dao.getConnection().prepareStatement(sql3);
            for (int i = 0; i < bookids.size(); i++) {
                ps.setInt(1, Integer.parseInt((String) finalnumber.get(i)));
                ps.setDouble(2, Double.parseDouble((String) bookprice.get(i)));
                ps.setString(3, order_id);
                ps.setString(4, (String) bookids.get(i));
                ps.executeUpdate();
            }
            response.sendRedirect("BackServlet");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (ps != null)
                    ps.close();
                if (statement != null)
                    statement.close();
                dao.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
