package servlet;

import DAO.BookDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Statement;

@WebServlet(name = "ChangeStateServlet")
public class ChangeStateServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        BookDao dao = new BookDao();
        Statement statement = null;
        try{
            String sql = "update orders set state = 1 where id ='"+id+"'";
            statement = dao.getConnection().createStatement();
            statement.executeUpdate(sql);
            response.sendRedirect("/rootorder.html");
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            try{
                if(statement !=null)
                    statement.close();
                dao.close();
            }catch (SQLException e){
                e.printStackTrace();
            }
        }
    }
}
