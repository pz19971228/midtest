package servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "BackServlet")
public class BackServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        ArrayList finalnumber= (ArrayList)session.getAttribute("finalnumber");
        ArrayList bookids= (ArrayList)session.getAttribute("bookids");
        ArrayList bookprice= (ArrayList)session.getAttribute("bookprice");
        ArrayList bookname= (ArrayList)session.getAttribute("bookname");
        finalnumber.clear();
        bookids.clear();
        bookname.clear();
        bookprice.clear();
        response.sendRedirect("/index.jsp");
    }
}
